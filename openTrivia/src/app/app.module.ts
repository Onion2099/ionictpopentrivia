import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

// Providers (services) Module 06
import { QuestionsService } from './providers/questions.service';
// Appel API Module 07
import{ HttpClientModule} from'@angular/common/http';
// Stockage Module 09 (Storage OFFLINE) debug possible seulement au niveau Build
import { IonicStorageModule } from '@ionic/storage';
// Stockage Module 09 (SQL Lite) debug possible
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule, 
    IonicModule.forRoot(), 
    AppRoutingModule,
    // Appel API Module 07
    HttpClientModule,
    // Stockage Module 09 (Storage OFFLINE)
    IonicStorageModule.forRoot(),
    
  ],
  providers: [
    StatusBar,
    SplashScreen,
    // Providers (services) Module 06
    QuestionsService,
    // Stockage Module 09 (SQL Lite)
    SQLite,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
