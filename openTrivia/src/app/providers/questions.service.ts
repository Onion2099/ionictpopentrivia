import { Injectable } from '@angular/core';
import { HttpClient } from'@angular/common/http';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class QuestionsService {

  constructor(private http: HttpClient) {
  }

  /**
   * getQuestions avec HttpClient (module 07)
   */
  // Au lieu de retourner un Array<Question>, on lui precise que l'on souhaite attendre 
  // le retour d'une Promesse avant de continuer son execution -> Promise<Array<Question>>
  async getQuestions(countQuestion: number, difficulty: string, category: string): Promise<Array<Object>> 
  {
    const res = await this.http.get("https://opentdb.com/api.php?amount=" + countQuestion + "&difficulty=" + difficulty + category).toPromise();
    if(res && res["results"]) 
    {
      return res["results"];
    } 
    else
    {
      // Contenu du Toast
      throwError("Impossible de récupérer les questions");
    }
  }

  /**
   * getQuestions en dure (jusqu'à Module 06)
   */
  // async getQuestions(countQuestion: number, difficulty: string) {
  //   return [
  //     {
  //       category:"film",
  //       type:"unique",
  //       difficulty:"easy",
  //       question:"Dans quel film peut-on entendre : 'qui c'est que tu traites de banane ? Banane !' ?",
  //       correct_answer:"Retour vers le Futur",
  //       incorrect_answers:["Harry Potter", "Starsky et Hutch", "Les Minions"]
  //     },
  //     {
  //       category:"film",
  //       type:"unique",
  //       difficulty:"easy",
  //       question:"Dans quel film on entend : 'vous voulez un whisky ? Juste un doigt. Vous voulez pas un whisky d’abord ?' ?",
  //       correct_answer:"La Cité de la peur",
  //       incorrect_answers:["Les Bronzés", "Astérix et Obélix", "OSS 117"]
  //     },
  //     {
  //       category:"film",
  //       type:"unique",
  //       difficulty:"easy",
  //       question:"Dans Harry Potter qui dit : 'Tu es un sorcier Harry !' ?",
  //       correct_answer:"Hagrid",
  //       incorrect_answers:["Vernon Dursley", "Ron Weasley", "Albus Dumbledore"]
  //     }
  //   ]
  // }

}
