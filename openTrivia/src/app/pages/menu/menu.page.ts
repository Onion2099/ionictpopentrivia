import { Component, OnInit } from '@angular/core';
import { ToastController, MenuController, ModalController } from '@ionic/angular';
import { Router } from '@angular/router';
// Perso
import { MenuModalPage } from '../modal/menu-modal/menu-modal.page';
// Sockage Module 09 (Storage OFFLINE)
import { Storage } from '@ionic/storage';
// Sockage Module 09 (SQL Lite) debug possible
import { throwError } from 'rxjs';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';

@Component({
  selector: 'app-home',
  templateUrl: 'menu.page.html',
  styleUrls: ['menu.page.scss'],
})

export class MenuPage implements OnInit {

  constructor(
    private router: Router,
    private toastCtrl: ToastController,
    private menuCtrl: MenuController,
    public modalController: ModalController,
    // Stockage Module 09 (Storage OFFLINE)
    private storage: Storage,
    // Stockage Module 09 (SQL Lite) debug possible - Injection du service sqliteService : SQLite
    private sqliteService : SQLite
  ) { }

  // Module 09 (SQL Lite) Chargement des données depuis la sauvegarde pardéfaut
  // Module 09 (SQL Lite) Si les clefs difficulte et pseudo existent, elles sont mises par défaut dans l'application
  ngOnInit() {
    this.createDatabase();
   }

  // Game Menu
  public nickname : string ='';
  public sauvegarder : boolean = false;
  public gameDifficulty : string = "medium";
  // Module 09 (SQL Lite) Conserver l’instance pour manipuler les requêtes
  private database : SQLiteObject;
  // Module 09 (SQL Lite) Conserver si le nickname est en base de données
  private existInDB : boolean = false;
  // Module 09 (SQL Lite) Conserver le score lu en base de données pour le transmettre à la page Quizz
  public score : number = 0;


  // Toast Message
  showToast(messageRegisterValid) {
    this.toastCtrl.create({
      message: messageRegisterValid,
      position: 'bottom',
      color: 'success',
      duration: 3000
    }).then(toast => toast.present());
  }

  // Modal choix Avatar
  async openModal() {
    const modal = await this.modalController.create({
      component: MenuModalPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }

  async dismissModal() {
    const modal = await this.modalController.dismiss({
      component: MenuModalPage,
      cssClass: 'my-custom-class'
    });
  }

  // Check Validation
  tryStartGame() {
    if (!this.nickname || this.nickname.length < 3)
    {
      this.showToast("Veuillez saisir un Pseudo d'au moins 3 caractères.");
      return;
    }
    // Sockage Module 09 (Storage OFFLINE)
    // this.storage.get(this.nickname).then((val) => {
    //   console.log("L'utilisateur a saisi ", val);
    // });

    // M9--Sauvegarde du pseudo et du niveau de difficultés.
    if (this.sauvegarder)
    {
      this.saveUserScoreInDatabase();
      //M9--Utilisation des valeurs en base de données si sauvegarde
      //M9--création d’un nouveau routage avec le paramètre score en plus
      this.router.navigate(["/quizzSQL", this.nickname, this.gameDifficulty, this.score]);
    }
    else
    {
      //M9–Si il n’ya pas sauvegarde en base de données –le score est par défaut à zéro
      //M9–utilisation du routage d’origine vers la page Game
      this.router.navigate(["/quizz", this.nickname, this.gameDifficulty]);
      this.showToast("Let's Go !");
    }
  }

  //////////////////// DATABASE ////////////////////
  // M9 -- Créer la table pour gérer la sauvegarde des données
  // Méthode appelée à chaque fois
  // si la table existe appel de la méthode loadDatabase pour charger les données
  createDatabase() {
  console.log("UseSQLite -- native")
  this.sqliteService.create(
    {
      name: 'dataOpenTrivia.db',
      location: 'default'
    }).then((db:SQLiteObject) => {
      this.database = db;
      db.executeSql('create table sauvegarde(nickname VARCHAR(32), gameDifficulty VARCHAR(32), score INTEGER)',[])
      .then(() => {
        console.log('Executed SQL');
      })
      .catch(e=>{
        console.log("Table existante", e);
        this.loadDatabase();
      });
    })
    .catch(e => console.log(e));
  }


  //M9--Sauvegardedupseudos'iln'yenapasenbasededonnéesou mise à jourpublic
  saveUserScoreInDatabase() {
    if(this.existInDB) {
      console.log("saveUserScoreInDatabase EXIST");
      
      let data = [ this.gameDifficulty, this.nickname ];
      this.database.executeSql('UPDATE saveUserScoreInDatabase = ? where pseudo = ?', data)
      .then(data => {
        console.log("updateSaveUserScoreInDatabase");
      });
    }
    else
    {
      console.log("saveUserScoreInDatabase NOT EXIST");
      
      let data = [this.nickname, this.gameDifficulty];
      this.database.executeSql('INSERT INTO sauvegarde (nickname, gameDifficulty, score) VALUES (?, ?, 0)', data)
      .then(data => {
        console.log("saveUserScoreInDatabase",data);
      });
    }
  }


  public loadDatabase() {
    let data = [this.nickname];
    this.existInDB = false;
    this.database.executeSql("SELECT * FROM sauvegarde WHERE nickname = ?", data).then((r) => {
      console.log(r);
      
      if(r.rows.length>0) 
      {
        this.existInDB = true;
        let item = r.rows.item(0);
        console.log ("Item=", item);
        this.nickname = item ['nickname'];
        this.gameDifficulty = item ['gameDifficulty'];
        this.score = item ['score'];
      }
      else 
      {
        throwError("Impossible de trouver les données en base");
      }
    })
    .catch(error=>{
      console.log(error);
    });
  }
}
