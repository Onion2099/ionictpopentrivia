import { Component, OnInit } from '@angular/core';
import { ToastController, NavController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { Router, ActivatedRoute} from'@angular/router';
// Providers (services)
import { QuestionsService } from '../../providers/questions.service';
// Models
import { Question } from '../../models/question.models';

@Component({
  selector: 'app-home',
  templateUrl: 'quizz.page.html',
  styleUrls: ['quizz.page.scss'],
})

export class QuizzPage implements OnInit {

  constructor(
    private toastCtrl: ToastController,
    private navCtrl: NavController,
    private router: Router,
    private activateRoute: ActivatedRoute,
    private questionsService: QuestionsService
  ) { }

  ngOnInit() {
    this.startGame();
    this.activateRoute.params.subscribe((params) =>
      {
        this.gameDifficulty= params["gameDifficulty"];
        this.nickname= params["nickname"];
      }
    );
  }

  // Game Menu
  nickname = "";
  sauvegarder = false;
  gameDifficulty = "";
  // Game Status
  gameInProgress = false;
  gameOver = false;
  score = 0;
  // Question / Answer
  questions = [];
  questionInProgress: Question;
  questionNumber = 0;
  nextQuestion = false;
  answerSelected = false;
  category = "";
  
  // Toast Message
  showToast(messageRegisterValid) {
    this.toastCtrl.create({
      message: messageRegisterValid,
      position: 'bottom',
      color: 'success',
      duration: 3000
    }).then(toast => toast.present());
  }

  // Game Init
  async startGame() {
    this.gameInProgress = true;
    this.gameOver = false;
    this.score = 0;
    this.nextQuestion = false;
    this.questionNumber = 0;
    this.questions = await this.questionsService.getQuestions(5, this.gameDifficulty, this.category);
    this.loadQuestion();
  }

  // Question
  loadQuestion() {
    this.questionInProgress = this.questions[this.questionNumber];
    this.questionInProgress["answers"] = [];
    
    for (let answer of this.questionInProgress["incorrect_answers"]) 
    {
      this.questionInProgress["answers"].push({correct:false, answer:answer});
    }

    this.questionInProgress["answers"].push({ correct:true, answer:this.questionInProgress["correct_answer"]});
    this.questionInProgress["answers"] = this.shuffleArray(this.questionInProgress["answers"]);
  }
  
  tryNextQuestion() {
    if (this.questionNumber < this.questions.length - 1) 
    {
      this.questionNumber++;
      this.nextQuestion = false;
      this.loadQuestion();
    }
  }

  // Random Array
  shuffleArray(array) {
    for (var i= array.length-1; i > 0; i--) {
      var j= Math.floor(Math.random() * (i + 1));
      var temp = array[i];
      array[i] = array[j];
      array[j] = temp;
    }
    return array;
  }

  // Answer
  tryToAnswer(reponse: string) {
  this.nextQuestion = true;
  if (reponse["correct"]) 
  {
    this.score ++;
  }
    if (this.questionNumber >= this.questions.length - 1) 
    {
      this.gameOver= true;
    }
  }

  tryEndGame() {
    this.gameInProgress = false;
    this.router.navigate(["/score", this.score]);
  }

}
