import { Question } from './question.models';

export class Game {
    
    public id: number;
    public questions: Array<Question>;
}