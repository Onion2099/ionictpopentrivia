export class Question {
    
    public id: number;
    public category: string;
    public type: string;
    public difficulty: string;
    public question: string;
    public rightAnswer: string;
    public answerA: string;
    public answerB: string;
    public answerC: string;
    public answerD: string;
    
}