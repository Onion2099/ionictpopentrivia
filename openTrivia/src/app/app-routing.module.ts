import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'menu',
    loadChildren: () => import('./pages/menu/menu.module').then( m => m.MenuPageModule)
  },
  {
    // Ajout Perso > Modal Menu Avatar
    path: 'menu-modal',
    loadChildren: () => import('./pages/modal/menu-modal/menu-modal.module').then( m => m.MenuModalPageModule)
  },
  {
    // Module 08 On précise dans notre URL les param embarqués 
    path: 'quizz/:nickname/:gameDifficulty',
    loadChildren: () => import('./pages/quizz/quizz.module').then( m => m.QuizzPageModule)
  },
  {
    // Module 08 On précise dans notre URL les param embarqués 
    path: 'score/:score',
    loadChildren: () => import('./pages/score/score.module').then( m => m.ScorePageModule)
  },
  {
    // Module 09 On précise dans notre URL les param embarqués + le score que l'on souhaite enregistrer en base
    path:'quizzSQL/:nickname/:gameDifficulty/:score',
    loadChildren:()=>import('./pages/quizz/quizz.module').then(m=>m.QuizzPageModule)
  },
  
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
